package com.mycrush;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.edmodo.cropper.CropImageView;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainActivity extends AppCompatActivity implements OnClickListener {
	ImageView mImageView_DP;
	EditText mEditText_NAME, mEditText_DOB, mEditText_RELATION;
	RadioGroup mGroup_Sex;
	Button mButton;
	int REQUEST_CODE_GALLARY = 5124, mMonth = 0, mYear = 0, mDay = 0,mRel = 500,mSex = 500;
	ImageLoader imageLoader;
	public static final String[] itmes = {"Friend","Colleague","ClassMate","Friend's Friend","Other"};
	boolean isDPset = false;
	Bitmap image;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getSupportActionBar().hide();
		init();

	}

	private void init() {
		mImageView_DP = (ImageView) findViewById(R.id.imgv_DP);
		mEditText_NAME = (EditText) findViewById(R.id.Edttxt_name);
		mEditText_DOB = (EditText) findViewById(R.id.edtxt_dob);
		mEditText_RELATION = (EditText) findViewById(R.id.Edttxt_relation);
		mGroup_Sex = (RadioGroup) findViewById(R.id.radiog_sex);
		mButton = (Button) findViewById(R.id.btn_submit);
		mImageView_DP.setOnClickListener(this);
		mEditText_DOB.setOnClickListener(this);
		mEditText_RELATION.setOnClickListener(this);
		mButton.setOnClickListener(this);
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration
				.createDefault(getApplicationContext()));
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.imgv_DP:
			Intent in = new Intent();
			in.setType("image/*");
			in.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(in, REQUEST_CODE_GALLARY);
			break;
		case R.id.edtxt_dob:
			DatePickerDialog.newInstance(new OnDateSetListener() {

				@Override
				public void onDateSet(DatePickerDialog datePickerDialog,
						int year, int month, int day) {
					
					mYear = year;
					mMonth = month+1;
					mDay = day;
					mEditText_DOB.setText("" + mYear + "/" + mMonth + "/" + mDay);
				}
			}, 1993, 10, 1, false).show(getSupportFragmentManager(), "DATE");
			break;
		case R.id.Edttxt_relation:
			
			AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
			dialog.setItems(itmes, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mEditText_RELATION.setText(itmes[which]);
					mRel = which;
					
				}
			});
			dialog.show();
			break;
		case R.id.btn_submit:
			if(TextUtils.isEmpty(mEditText_NAME.getText().toString())){
				Maketoast(getApplicationContext(), "Enter Name First");
				return;
			}else if(!isDPset){
				Maketoast(getApplicationContext(), "Set Your Crush or a Picture Related to your Crush");
				return;
			}else if(mYear == 0 || mMonth == 0 || mDay == 0){
				Maketoast(getApplicationContext(), "Choose Date of Birth of your Crush.");
				return;
			}else if(mRel == 500){
				Maketoast(getApplicationContext(), "Enter Your Relation to Your Crush.");
				return;
			}else{
				AlertDialog.Builder fdialog = new AlertDialog.Builder(MainActivity.this);
				fdialog.setMessage("Before Begain,\nLet's discuss some rule of this Game,\nNow we will ask some Questions according to your Crush and Your Result will be depend on your Answers.\nSo The Rules are Follows.\n\n1. There is No Back Option so Answer the questions carefully.\n\n2. You Can ask your friends for help coz It's a Game not an exam.\n\n3. Results May be vary Acording to your Crush Info which you Provided to us or according to answers of Questions.\n\n\nSo Are You ready?\n");
				fdialog.setPositiveButton("Yes, I'am ready", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						int id = mGroup_Sex.getCheckedRadioButtonId();
						if(id==R.id.radio_m)
							mSex = 1;
						else
							mSex = 0;
						Intent i1 = new Intent(MainActivity.this,QuestionsActivity.class);
						i1.putExtra(QuestionsActivity.INTENT_KEY_DAY, mDay);
						i1.putExtra(QuestionsActivity.INTENT_KEY_MONTH, mMonth);
						i1.putExtra(QuestionsActivity.INTENT_KEY_YEAR, mYear);
						i1.putExtra(QuestionsActivity.INTENT_KEY_SEX, mSex);
						i1.putExtra(QuestionsActivity.INTENT_KEY_NAME, mEditText_NAME.getText().toString());
						i1.putExtra(QuestionsActivity.INTENT_KEY_RELATION, itmes[mRel]);
						startActivity(i1);
						}
				});
				fdialog.show();
			}
			
			break;
		}

	}
	public static void Maketoast(Context context, String text){
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if (arg0 == REQUEST_CODE_GALLARY && arg1 == RESULT_OK && arg2 != null) {
			Uri uri = arg2.getData();
			String[] filepathcouloum = { MediaStore.Images.Media.DATA };
			Cursor cursor = getContentResolver().query(uri, filepathcouloum,
					null, null, null);
			cursor.moveToFirst();
			int columnindex = cursor
					.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			String picturepath = cursor.getString(columnindex);
			cursor.close();
			Bitmap bitmap = imageLoader.loadImageSync("file://" + picturepath);
			if (bitmap != null) {
				//showcropdialog(bitmap);
				mImageView_DP.setImageBitmap(bitmap);
				isDPset = true;
			}
		}

		super.onActivityResult(arg0, arg1, arg2);
	}

	private void showcropdialog(Bitmap bitmaptocrop) {
		final Dialog dialog = new Dialog(MainActivity.this,
				R.style.Theme_AppCompat_DialogWhenLarge);
		dialog.setContentView(R.layout.crop_dialog);
		final CropImageView cropImageView = (CropImageView) dialog
				.findViewById(R.id.CropImageView);
		Button rotate1 = (Button) dialog.findViewById(R.id.btn_dialog_rotate1);
		Button rotate2 = (Button) dialog.findViewById(R.id.btn_dialog_rotate2);
		Button crop = (Button) dialog.findViewById(R.id.btn_dialog_crop);
		cropImageView.setImageBitmap(bitmaptocrop);
		cropImageView.setAspectRatio(100, 100);
		rotate1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cropImageView.rotateImage(90);
			}
		});
		rotate2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cropImageView.rotateImage(-90);
			}
		});
		crop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mImageView_DP.setImageBitmap(cropImageView.getCroppedImage());
				dialog.dismiss();
				CroppedBitmap.setCroBitmap(cropImageView.getCroppedImage());
				isDPset = true;
			}
		});
		dialog.show();
	}
	
}
