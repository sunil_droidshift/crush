package com.mycrush;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class Splash extends Activity {
	Button mButton;
	ProgressBar bar;
	final int totalProgressTime = 100;
	int which = 0;
	RelativeLayout mLayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_in_left);
		mButton = (Button) findViewById(R.id.btn_splash_go);
		bar = (ProgressBar) findViewById(R.id.bar_splash_progress);
		mLayout = (RelativeLayout)findViewById(R.id.splashmain);
		bar.setMax(totalProgressTime);
		mButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mButton.setVisibility(View.GONE);
				bar.setVisibility(View.VISIBLE);
				final Thread t = new Thread() {

					@Override
					public void run() {

						int jumpTime = 0;
						while (jumpTime < totalProgressTime) {
							try {
								sleep(10);
								jumpTime++;
								bar.setProgress(jumpTime);
								runOnUiThread(new Runnable() {
									
									@SuppressWarnings("deprecation")
									@SuppressLint("NewApi")
									@Override
									public void run() {
										if(which == 0){
											if(android.os.Build.VERSION.SDK_INT >=16)
												mLayout.setBackground(getResources().getDrawable(R.drawable.splash_bg2));
											else
												mLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.splash_bg2));
											which = 1;
										}else{
											if(android.os.Build.VERSION.SDK_INT >=16)
												mLayout.setBackground(getResources().getDrawable(R.drawable.splash_bg));
											else
												mLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.splash_bg));
											which = 0;
										}	// TODO Auto-generated method stub
										
									}
								});
								if(jumpTime == 100){
									runOnUiThread(new Runnable() {
										
										@Override
										public void run() {
											holla();
											
										}
									});
								}
									
								
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				};
				t.start();

			}

		});

	}
	private void holla() {
		startActivity(new Intent(Splash.this,MainActivity.class));
		overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
		finish();
		
	}
	
}