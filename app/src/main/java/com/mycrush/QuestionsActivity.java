package com.mycrush;

import java.util.ArrayList;

import com.meg7.widget.SvgImageView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

public class QuestionsActivity extends Activity {
	;
	public static String INTENT_KEY_NAME = "INTENT_KEY_NAME";
	public static String INTENT_KEY_RELATION = "INTENT_KEY_RELATION";
	public static String INTENT_KEY_YEAR = "INTENT_KEY_YEAR";
	public static String INTENT_KEY_MONTH = "INTENT_KEY_MONTH";
	public static String INTENT_KEY_DAY = "INTENT_KEY_DAY";
	public static String INTENT_KEY_SEX = "INTENT_KEY_SEX";
	ArrayList<String> Q4M, Q4F, Q2A, AOQ, Answers;
	SvgImageView image;
	String name, relation;
	int year, month, day, sex;
	Intent i1;
	TextView textViewQ;
	int onques = 0;
	final int maxques = 12; // Should be less then total Questions
	Button nextbButton;
	TextView textView_prog;
	RadioGroup answers;
	ProgressDialog pd1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_questions);
		init();
		addquestolist(Q4M, Q4F);
		if (sex == 1) {
			Q2A = Utils.getrandomquestion(Q4M, maxques);
			String quesssss = "Q." + (onques + 1) + " " + Q2A.get(onques);
			textViewQ.setText(quesssss.substring(0, quesssss.length()-1));
		} else {
			Q2A = Utils.getrandomquestion(Q4F, maxques);
			String quesssss = "Q." + (onques + 1) + " " + Q2A.get(onques);
			textViewQ.setText(quesssss.substring(0, quesssss.length()-1));
		}
		AOQ = new ArrayList<String>();
		Answers = new ArrayList<String>();
		for (int i = 0; i < Q2A.size(); i++) {
			AOQ.add(String.valueOf(Q2A.get(i).charAt(Q2A.get(i).length() - 1)));

		}

		nextbButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (answers.getCheckedRadioButtonId() == R.id.radio_ques_yes)
					Answers.add("Y");
				else
					Answers.add("N");
				if (onques < maxques - 1) {
					onques++;
					textView_prog.setText("" + (onques * 100) / maxques
							+ "% Done");
					String quesssss = "Q." + (onques + 1) + " " + Q2A.get(onques);
					textViewQ.setText(quesssss.substring(0, quesssss.length()-1));
					/*textViewQ.setText("Q." + (onques + 1) + " "
							+ Q2A.get(onques));*/
				} else {
					onques++;
					textView_prog.setText("" + (onques * 100) / maxques
							+ "% Done");
					nextbButton.setEnabled(false);
					for (int j = 0; j < AOQ.size(); j++) {
						Log.d("Ans:UserAns of Ques " + (j + 1), AOQ.get(j)
								+ ":" + Answers.get(j));
					}
					new Analyse().execute();
				}

			}
		});
	}

	public class Analyse extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			if(pd1.isShowing())
				pd1.dismiss();
			Intent i1 = new Intent(QuestionsActivity.this,ResultActivity.class);
			i1.putExtra(ResultActivity.INTENT_KEY_ARRAYLIST_USERANS, Answers);
			i1.putExtra(ResultActivity.INTENT_KEY_ARRAYLIST_ANS, AOQ);
			startActivity(i1);
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				Thread.sleep(5000);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			pd1 = ProgressDialog.show(QuestionsActivity.this, "Analysing",
					"Please Wait While we Analyse Your result", false, false,
					new OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							dialog.dismiss();
							finish();

						}
					});
			super.onPreExecute();
		}

	}

	private void init() {

		i1 = getIntent();
		Q4F = new ArrayList<String>();
		Q4M = new ArrayList<String>();
		name = i1.getStringExtra(INTENT_KEY_NAME);
		relation = i1.getStringExtra(INTENT_KEY_RELATION);
		year = i1.getIntExtra(INTENT_KEY_YEAR, 1993);
		month = i1.getIntExtra(INTENT_KEY_MONTH, 11);
		day = i1.getIntExtra(INTENT_KEY_DAY, 1);
		sex = i1.getIntExtra(INTENT_KEY_SEX, 2);
		textViewQ = (TextView) findViewById(R.id.txt_ques);
		nextbButton = (Button) findViewById(R.id.btn_nextQ);
		textView_prog = (TextView) findViewById(R.id.txt_progress);
		answers = (RadioGroup) findViewById(R.id.Radio_G_answer);
		image = (SvgImageView)findViewById(R.id.svg_img);
		image.setImageBitmap(CroppedBitmap.getCroBitmap());
		Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_img);
		image.startAnimation(anim);
	}

	private void addquestolist(ArrayList<String> Male, ArrayList<String> FeMale) {
		Male.add("Does he Smile Whenever you look at him?Y");
		Male.add("Does he look for an Excuse to talk to you?N");
		Male.add("Does he Care For You Most?Y");
		Male.add("Does he always try to make you smile?Y");
		Male.add("If he is going for ant trip or tour, does he asks you to join or not?Y");
		Male.add("Does he like guy's more then girl's?N");
		Male.add("Does he always respect you and your decision?Y");
		Male.add("Does he like to spend time with you?Y");
		Male.add("Does he try to insult you infront of others?N");
		Male.add("Does he feel jealous when you meet you Male friends?Y");
		Male.add("Does he show intrest on your friends instead of you?N");
		Male.add("If you call him at night and ask to help you in anything, will he do?Y");
		Male.add("Does he deserve you?Y");
		Male.add("Does he always looking for a Chance to talk or cantact you?Y");
		Male.add("Has he ever commented on your attractiveness with a word like 'hot', 'beautiful', 'gorgeous', 'sexy' or 'cute'?Y");
		Male.add("Is he in a relationship with anyone else right now?N");
		Male.add("Do you think he likes you? Be honest�!Y");
		Male.add("Has he given you any reason to doubt that he's interested in you?Y");
		Male.add("Does he ever talk or said about being interested in other women?N");
		Male.add("Has he ever said, for any reason, that he isn't looking for a relationship or can't be in a relationship right now?N");
		Male.add("Does he come visit you often (at your Desk, Locker, Room, Kitchen or anywhere)?Y");
		Male.add("Has he ever stared at you first when you walk by or pass by?Y");
		Male.add("Does he hang out with a lot of different girls? (is he a player)N");
		Male.add("Have you guys Have ever kissed or more then that?N");
		Male.add("Is he afraid of talking to you infront of her Female Friends?N");

		// ////////////////////////////////

		FeMale.add("Does she Smile Whenever you look at her?Y");
		FeMale.add("Does she look for an Excuse to talk to you?N");
		FeMale.add("Does she Care For You Most?Y");
		FeMale.add("Does she always try to make you smile?Y");
		FeMale.add("If she is going for any trip or tour, does she asks you to join or not?Y");
		FeMale.add("Does she like girl's more then guy's?N");
		FeMale.add("Does she always respect you and your decision?Y");
		FeMale.add("Does she like to spend time with you?Y");
		FeMale.add("Does she try to insult you infront of others?N");
		FeMale.add("Does she feel jealous when you meet you Female friends?Y");
		FeMale.add("Does she show intrest on your friend's instead of you?N");
		FeMale.add("If you call her at night and ask to help you in anything, will she do?Y");
		FeMale.add("Does she deserve you?Y");
		FeMale.add("Does she always looking for a chance to talk or cantact you?Y");
		FeMale.add("Has she ever commented on your attractiveness with a word like 'Sexy', 'Hunk', 'Smart', 'cool' or 'cute'?Y");
		FeMale.add("Is she in a relationship with anyone else right now?N");
		FeMale.add("Do you think she likes you? Be honest�!Y");
		FeMale.add("Has she given you any reason to doubt that she's interested in you?Y");
		FeMale.add("Does she ever talk or said about being interested in other men?N");
		FeMale.add("Has she ever said, for any reason, that she isn't looking for a relationship or can't be in a relationship right now?N");
		FeMale.add("Does she come visit you often (at your Desk, Locker, Room, Kitchen or anywhere)?Y");
		FeMale.add("Has she ever stared at you first when you walk by or pass by?Y");
		FeMale.add("Does she hang out with a lot of different guys? (is she a player)N");
		FeMale.add("Have you guys Have ever kissed or more then that?N");
		FeMale.add("Is she afraid of talking to you infront of her Male friends?N");

	}
}
