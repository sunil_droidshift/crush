package com.mycrush;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class ResultActivity extends Activity {
	public static String INTENT_KEY_ARRAYLIST_USERANS = "userans";
	public static String INTENT_KEY_ARRAYLIST_ANS = "ans";
	TextView result;
	Intent i1;
	int correctanswer = 0;
	ArrayList<String> UserAnswer, CorrectAnswer;
	CircularProgressBar progressBar;
	ImageView resultimage;
	Button sharebtn;
	RelativeLayout shareimage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
		init();
		for (int i = 0; i < CorrectAnswer.size(); i++) {
			if (CorrectAnswer.get(i).equals(UserAnswer.get(i)))
				correctanswer++;
		}
		int urresult =  (correctanswer * 100) / (CorrectAnswer.size() + 1);
		new Handleprogress(urresult).execute();
		sharebtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				shareimage.buildDrawingCache();
				Bitmap image = shareimage.getDrawingCache();
				File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString());
				File imagefile = new File(dir, "IMG_"+String.valueOf(System.currentTimeMillis())+".jpg");
				FileOutputStream stream;
				try {
					stream = new FileOutputStream(imagefile);
					image.compress(CompressFormat.JPEG, 100, stream);
					stream.flush();
					stream.close();
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(), "Unable to Share Image.", Toast.LENGTH_SHORT).show();
					
				}
				Uri uri = Uri.fromFile(imagefile);
				Intent i1 = new Intent(Intent.ACTION_SEND);
				i1.setType("image/*");
				i1.putExtra(Intent.EXTRA_TEXT, "I Get This Result From and Awsome app called My Crush, Get it from here:-\n https://play.google.com/store/apps/details?id="+getPackageName());
				i1.putExtra(Intent.EXTRA_STREAM, uri);
				startActivity(i1);
			}
		});
	}

	private void init() {
		i1 = getIntent();
		UserAnswer = i1.getStringArrayListExtra(INTENT_KEY_ARRAYLIST_USERANS);
		CorrectAnswer = i1.getStringArrayListExtra(INTENT_KEY_ARRAYLIST_ANS);
		progressBar =  findViewById(R.id.circularprogressbar2);
		result = findViewById(R.id.result_txt);
		resultimage = findViewById(R.id.resultimage);
		resultimage.setImageBitmap(CroppedBitmap.getCroBitmap());
		sharebtn = findViewById(R.id.share_btn);
		sharebtn.setEnabled(false);
		shareimage = findViewById(R.id.result_image);
		

	}

	public class Handleprogress extends AsyncTask<Void, Void, Void> {
		int max,i;

		public Handleprogress(int max) {
			this.max = max;
		}

		@Override
		protected Void doInBackground(Void... params) {
			for (i = 0; i < max; i++) {
				try {
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							progressBar.setProgress(i);
							progressBar.setTitle(""+i+"%");
							
						}
					});
					
					Thread.sleep(30);
				} catch (final Exception e) {
					Log.d("ERROR",e.toString());
					
				}
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if(max <50){
					progressBar.setSubTitle("Bad");
					progressBar.setSubTitleColor(Color.RED);
					ResultActivity.this.result.setText("I'm sorry, but you both are just friends. Talk to your crush a little more and If your will able to gain Intrest into your Crush then hope you both make a new love story. :)");
					}else if(max<75 && max >50){
						progressBar.setSubTitle("Good");
						ResultActivity.this.result.setText("Does your crush love you? or its just a Crush, or you are just dreaming, Actually i'm Confused too, you should go and have a talk to your crush, Finger crossed ;)");
						progressBar.setSubTitleColor(Color.GREEN);
					}else{
						progressBar.setSubTitle("Wow!");
						progressBar.setSubTitleColor(Color.BLUE);
						ResultActivity.this.result.setText("Yes! OMG! Yes! Congrats! Your Crush DOES love you! Just Go and ask this thing to your crush, Oh Yes, I made One More Love Story, Share me to Your Frinds to let me make some more Love Story.");
					}
					sharebtn.setEnabled(true);
					}	
			});
			super.onPostExecute(result);
		}
	}
	private Bitmap Circlebitmap(Bitmap bitmapimg) {
		Bitmap output = Bitmap.createBitmap(bitmapimg.getWidth(),
                bitmapimg.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmapimg.getWidth(),
                bitmapimg.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmapimg.getWidth() / 2,
                bitmapimg.getHeight() / 2, bitmapimg.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmapimg, rect, rect, paint);
        return output;

	}
}
