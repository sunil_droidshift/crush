package com.mycrush;

import java.util.ArrayList;
import java.util.Collections;

public class Utils {
	public static ArrayList<String> getrandomquestion(ArrayList<String> from,int howmany) {
		Collections.shuffle(from);
		
		ArrayList<String> returnlist = new ArrayList<String>();
		for (int i = 0; i < howmany; i++) {
			returnlist.add(from.get(i));
		}
		return returnlist;
	}

}
