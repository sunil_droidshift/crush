package com.mycrush;

import android.graphics.Bitmap;

public class CroppedBitmap {
	public static Bitmap CroBitmap;

	/**
	 * @return the croBitmap
	 */
	public static Bitmap getCroBitmap() {
		return CroBitmap;
	}

	/**
	 * @param croBitmap the croBitmap to set
	 */
	public static void setCroBitmap(Bitmap croBitmap) {
		CroBitmap = croBitmap;
	}

}
